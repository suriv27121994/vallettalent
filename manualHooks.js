const prevCountRef = useRef();
useEffect(() => {
  prevCountRef.current = count;
}, [count]);
const prevCount = prevCountRef.current;
