// function stockPairs(stocksProfit, target) {
//   // Write your code here

//   const pairs = [];
//   stocksProfit.forEach((profit1, idx1) => {
//     stocksProfit.forEach((profit2, idx2) => {
//       if (idx1 === idx2) return;

//       if (profit1 + profit2 === target) {
//         pairs.push([profit1, profit2]);
//       }
//     });
//   });

//   const totalPairs = pairs.map((pair) => {
//     return [...pair].sort().join("");
//   });

//   const distinctPailogrs = new Set(totalPairs);
//   console.log(distinctPairs);
//   return distinctPairs.size;
// }

// console.log(stockPairs());

//===================

function stockPairs(stocksProfit, target) {
  // Write your code here
  const pairedValues = {};
  let count = 0;

  for (let i = 0; i < stocksProfit.length; i += 1) {
    //stocksProfit[0] = 5
    if (!pairedValues[stocksProfit[i]]) {
      for (let j = i + 1; j < stocksProfit.length; j += 1) {
        // stocksProfit[1] = 7
        if (stocksProfit[i] + stocksProfit[j] === target) {
          // console.log('pairedValues', pairedValues);
          if (
            !pairedValues[stocksProfit[i]] &&
            !pairedValues[stocksProfit[j]]
          ) {
            pairedValues[stocksProfit[i]] = true;
            pairedValues[stocksProfit[j]] = true;
            count += 1;
            break;
          }
        }
      }
    }
  }
  return count;
}

console.log(stockPairs());