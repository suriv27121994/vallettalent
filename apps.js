const axios = require('axios');  
const {React, useLayoutEffect, useState } = require('react');
//import axios from "axios";


const App = () => {
  const [data, setData] = useState({ hits: [] });

  useLayoutEffect(async () => {
    const result = await axios("http://example.com/api/v1/search?query=react");
    setData({ hits: result.data });
  });

  return (
    <ul>
      {data.hits.map((item) => (
        <li key={item.id}>{item.name}</li>
      ))}
    </ul>
  );
};

export default App;
