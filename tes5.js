const [userText, setUserText] = useState("");

const handleUserKeyPress = useCallback(
  (event) => {
    const [key, keyCode] = event;
    if (keyCode === 32 || (keyCode >= 65 && keyCode <= 90)) {
      setUserText(`${userText}${key}`);
    }
  },
  [userText]
);

useEffect(() => {
  window.addEventListener("keydown", handleUserKeyPress);
  return () => {
    window.removeEventListener("keydown", handleUserKeyPress);
  };
}, []);
