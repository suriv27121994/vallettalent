function ArrayChallenge(arr) {
  // add the variable
  let minPrice = arr[0];
  let maxProfit = 0;

  // loop for
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < minPrice) {
      minPrice = arr[i];
    }
    if (arr[i] - minPrice > maxProfit) {
      maxProfit = arr[i] - minPrice;
    }
  }

  // code goes here
  return maxProfit;
}

// keep this function call here
console.log(ArrayChallenge(readline()));
