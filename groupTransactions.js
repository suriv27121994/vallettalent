function groupTransactions(transactions) {
  let transaction_dict = {};

  for (let i = 0; i < transactions.length; ++i) {
    if (transactions[i] in transaction_dict) {
      transaction_dict[transactions[i]] += 1;
    } else {
      transaction_dict[transactions[i]] = 1;
    }
  }

  let count_dict = {};

  for (const [key, value] of Object.entries(transaction_dict)) {
    if (value in count_dict) {
      count_dict[value].push(key);
    } else {
      count_dict[value] = [key];
    }
  }

  for (const [key, value] of Object.entries(count_dict)) {
    count_dict[key].sort();
  }

  let res = [];
  let sorted_keys = Object.keys(count_dict).sort(function (a, b) {
    return b - a;
  });

  for (let i = 0; i < sorted_keys.length; ++i) {
    let key = sorted_keys[i];

    for (let j = 0; j < count_dict[key].length; ++j) {
      res.push(count_dict[key][j] + " " + key);
    }
  }

  return res;
}
