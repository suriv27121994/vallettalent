function MinWindowSubstring(strArr) {
  // code goes here
  let min = null;
  let n = strArr[0];
  let k = strArr[1];
  for (let i = 0; i < n.length; i++) {
    for (let j = i + 1; j <= n.length; j++) {
      let sub = n.slice(i, j);
      if (check_K(sub, k)) {
        if (min === null || sub.length < min.length) {
          min = sub;
        }
      }
    }
  }
  return min;
}

function check_K(sub, k) {
  let h = k.split("");
  for (let i = 0; i < sub.length; i++) {
    let char = sub[i];
    let index = h.indexOf(char);
    if (index > -1) {
      h.splice(index, 1);
    }
  }
  if (h.length === 0) {
    return true;
  } else {
    return false;
  }
}

// keep this function call here
//console.log(MinWindowSubstring(readline()));

console.log(MinWindowSubstring(["ahffaksfajeeubsne", "jefaa"])); // aksfaje
